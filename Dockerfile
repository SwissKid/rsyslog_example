from alpine
ENTRYPOINT [ "/bin/ash" ]
CMD ["/start.sh"]
RUN apk update && apk add rsyslog
COPY ./start.sh /
COPY ./etc/ /etc/
